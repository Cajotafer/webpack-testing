const waitTime = new Promise((todoOK, todoMal) => {
    setTimeout(() => {
        todoOK('Han pasado los 3 segundos de espera')
    }, 3000)
})

module.exports = {
    firstMessage: 'Hola mundo desde un módulo',
    delayedMessage: async () => {
        const message = await waitTime
        console.log(message)
    }
}