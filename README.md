WP es un Module Bundler for Modern JS Applications, es decir un empaquetador de módulos para el desarrollo de aplicaciones modernas en JavaScript.

**¿Por qué usar Webpack? **

Aunque hay otras alternativas WP es un la forma mas sofisticada para cargar y transformar módulos. WP trae todas las formas de importación de módulos, en resumen trae lo mejor de todos los mundos.

- Entrypoints - múltiples puntos de entrada a tus aplicaciones - archivos iniciales, tienes uno por cada pagina que vayas a usar. Puedes tener multiples entrypoints.

- OUTPUT. Si bien le decimos cual es el archivo fuente, debemos decirle que hacer con eso y donde ponerlo, porque no queremos mezclar los archivos finales que lee el navegador con los archivos fuente.

- Loaders. Nos ayudan a cargar todo tipo de formato de archivos.

- Plugins. nos ayudan a extender las caracteristicas de WP, por ejemplo comprimir archivos, dividir nuestros modulos en chunks, etc.

*WP es developer experience.*

---

## Instalación

Como siempre, es necesario iniciar con la creación del **package.json** con

```
npm init
```

Luego instalamos el paquete de webpack con 

```
sudo npm install -D webpack webpack-cli
```

Instalamos dos cosas:

1. Un cliente que nos apoyará con comandos desde la terminal.
2. Un software que corre en el SO y que es el encargado de cargar y empaquetar los recursos.

---

La forma ideal de generar bundles es a través de scripts configurados en el package.json, estos scripts pueden ser ejecutados desde la terminal usando 

```
npm run [script]
```

ej: 

```
...
"script": {
    "build": "webpack index.js -o bundle.js --mode development"
}
```

Este script puede ser ejecutado al escribir en la terminal 

```
npm run build
```

y lo que hace es empaquetar el archivo **index.js** y exportarlo hacia **bundle.js**. *El --mode puede ser "development" o "production".

## Creando un webpack.config.js

Este archivo servirá como configuración para nuestros scripts de webpack. La configuración de este archivo será **un objeto**:

```
module.exports = {...}
```
Este objeto que se exporta como módulo debe tener una configuración mínima preestablecida.

1. **mode**: debe ser 'development' o 'production'.
2. **entry**: contiene la ruta y nombre del archivo de entrada '(...)./index.js'.
3. **output**: es un objeto más que puede tener múltiples configuraciones. Como base se puede definir dentro el **filename** que será el nombre del archivo de salida.

```
const path = require('path')

module.exports = {
    mode: 'development',
    entry: './index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    }
}
```

En este caso el **require('path')** lo que hace es importar el módulo path the node.js para luego con **path.resolve(__dirname, ...)** resolver la ruta actual del archivo para definir donde se exportará el archivo de salida, ya que esta ruta relativa cambia de acuerdo al sistema. 

Para ejecutar esto agregamos un nuevo script

```
"build:test": "webpack"
```
Con sólo llamar a webpack, webpack buscará el archivo webpack.config para correr su contenido. Se pueden hacer otros archivos .config e igualmente pueden ser llamados por otros scripts.

Si se quiere cambiar el archivo webpack.config que se ejecutará en el script del package, deberíamos escribir algo como esto:

```
"build:other": "webpack --config ./folder/webpack.config.js"
```
De esta forma estamos ingresando a otro directorio y ejecutando el archivo webpack.config contenido en el.

## Loaders

Los loaders se van a definir en el archivo webpack.config.js, como objetos dentro de un arreglo **rules** que es un elemento del objeto **module**.

```
...,
module: {
    rules: [
        {
            test: que tipo de archivo quiero reconocer,
            use: que loader se va a encargar del archivo 
        }
    ]
}
```

Todo loader que vayamos a usar con webpack debe ser previamente instalado como dependencia. En este caso vamos a instalar los loaders necesarios para procesar .css.

Ubicados en el directorio de webpack:

```
npm install -D style-loader css-loader
```

Una vez instaladas las dependencias ya podemos usar estos loaders en nuestro webpack.config.

```
module: {
    rules: [
        {
            test: /\.css$/,
            use: [
                'style-loader',
                'css-loader'
            ]
        }
    ]
}
``` 
**Importante**: el orden en el que declaras los loaders afecta el orden de ejecución. Debes colocarlos en orden de ejecución desde el final hasta el inicial. Por ejemplo, siempre se debe ejecutar primero el css-loader porque este lee el css y lo interpreta, y posteriormente el style-css que se encarga de imprimir los estilos.


Ahora webpack puede interpretar css con **css-loader** y también imprimirlo gracias al **style-loader**. Para ver los efectos de esto, en el **index.js** debemos importar la hoja de estilos:

```
index.js
---

import './stilos.css'
```
Y en el html debemos importar nuestro **bundle.js** desde la ubicación de salida.

```
<script src="./dist/bundle.js"></script> 
```
---

Si quisieras utilizar Sass, la documentación está muy completa: https://github.com/webpack-contrib/sass-loader tienes que installar 2 nuevas dependencias:
- sass-loader
- node-sass o sass (sass instala dart-sass. Se prefiere node-sass por eficiencia pero en caso de errores se puede usar la versión de dart tomando en cuenta que esta requiere agregar una opción de implementación)

Así quedarían estos loaders usando sass (estoy usando la versión dart porque la versión node-sass me dió ciertos errores)

```
 module: {
    rules: [
        {
            test: /\.(css|scss)$/,
            use: [
                'style-loader',
                'css-loader',
                {
                    loader: 'sass-loader',
                    options: {
                        implementation: require('sass')
                    }
                }
            ]
        }
    ]
}
```

---

Así como se le puede dar soporte a **sass**, también se puede hacer con **[stylus](https://github.com/shama/stylus-loader)**, **[less](https://github.com/webpack-contrib/less-loader)** y **[postCss](https://github.com/postcss/postcss-loader)** (También necesita el core y el loader *postcss* y *postcss-loader*).



## Extraer CSS a un archivo independiente

Al usar **style-loader**, todo el css que identificó **css-loader** será impreso será impreso en el html desde el JS. Para extraer este css e imprimirlo en un archivo externo y no depender del JS para renderizar vistas al usuario, necesitamos un plugin: **mini-css-extract-plugin** (https://github.com/webpack-contrib/mini-css-extract-plugin).

Debemos instalar la librería por npm con:

```
npm install mini-css-extract-plugin -D
```

Una vez instalada la librería como dependencia de desarrollo ya podemos usar el plugin con webpack. Primero debemos importar mini-css-extract-plugin en el webpack.config.js de la siguiente manera:

```
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
```

Tenemos que crear el nuevo objeto plugin de la clase MiniCssExtractPlugin y especificar sus opciones.

```
plugins: [
    new MiniCssExtractPlugin({
        filename: '[name].css'
    })
],
```

Donde **filename** es el archivo de salida.

Por último, en las reglas para procesar el css, **MiniCssExtractPlugin.loader** sustituye al **style-loader** porque ya no queremos imprimir el css en el html sino exportarlo a su archivo independiente.

```
 module: {
        rules: [
            {
                test: /\.(css|scss)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    //'style-loader', // creates style nodes from JS strings
                    'css-loader', // translates CSS into CommonJS
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require('sass')
                        }
                    }
                ]
            }
        ]
    }
}
```

## Multiples entry-points

Puede que necesitemos tener múltiples entry-points y generar varios outputs. Para esto **entry** pasa a ser un objeto que contiene los diferentes entry-points.

```
entry: {
    index: path.resolve(__dirname, 'src/js/index.js'),
    home: path.resolve(__dirname, 'src/js/home.js')
},
```

Ahora no puede definirse un sólo archivo de salida de esta forma, por tanto debemos hacer que la salida sea "dinámica" y puede hacerse dándole el mismo nombre que su entry-point:

```
output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js'
},
```

**[name].js** hace que cada entry-point genere su propio output conservando el nombre.

## devServer - Servidor de desarrollo

Webpack tiene la opción de autocompilado cada vez que se realiza un cambio en los archivos, usando la bandera **--watch** en nuestro script o agregando **watch: true** en el archivo de configuración, sin embargo, también podemos levantar un servidor local que no solo compila en tiempo real sino que también actualiza el navegador con los cambios.

Para levantar el servidor tenemos que instalar **webpack-dev-server** con npm:

```
npm install webpack-dev-server -D
```

Así de simple, para ejecutar debemos agregar un nuevo script ya que no necesitamos ejecutar **webpack** sino **webpack-dev-server**.

```
  "scripts": {
    "build:test": "webpack",
    "build:dev": "webpack-dev-server"
  },
```

Al devServer también se le pueden agregar diversas banderas, seguramente lo que más necesitarémos será

- --open : Cuando se ejecuta el script abre el navegador inmediatamente
- --config ./directorio/webpack.dev.config.js : Se puede personalizar el archivo de configuración que recibirá
- --port [number] : Define que puerto usar
- --content-base [file/directory/url/port] : Define la ruta base para los contenidos
- --https : Inicia el servidor webpack-dev-server sobre el protocolo HTTPS.

Si queremos agregar estas opciones en el webpack.dev.config.js en lugar de agregarlas en el script como banderas:

Debemos agreagar un nuevo elemento **devServer**

```
devServer: {
    port: 9000,
    publicPath: "/dist/",
    open: true,
    contentBase: path.join(__dirname, 'dist'),
},
```

*Es muy importante definir el publicPath, desde donde serán servidos los archivos*

## Babel

Babel ayuda a transpilar el .js a la versión de ES que definas para dar mayor soporte a los navegadores.
No es indispensable pero es indiscutiblemente útil para poder usar sintaxis de ES6+ sin problemas.

## Url-loader

Este loader se encargará de resolver las url's de imágenes y otros archivos multimedia volviéndolos cadenas de texto incrustadas en el .js o .css, evitando así más peticiones http.

Esta característica es genial cuando tienes imagenes pequeñas, imagenes de gran peso pueden ralentizar el renderizado y por ende, la experiencia del usuario.

Primero debes instalar la dependencia con:

```
npm install url-loader -D
```

Una vez instalada la librería ya puedes incluir el loader en las reglas del módulo:

```
{
    test: /\.(jpg|jpeg|png|gif)$/,
    use: {
        loader: 'url-loader',
        options: {
            limit: 100000, // Reflejado en bites
        } 
    }
},
```
En este caso usamos el loader para los archivos de imagen y establecemos un límite de 100kb, si la imagen pasa de este peso debería ser manejada por el file-loader *(a continuación)*

Con **url-loader** puedes también soportar fuentes, es una excelente opción para evitar las peticiones constantes que generan las fuentes. En este caso tendrás que descargar el kit de fuentes y llamarlas desde el css con **@font-face**.

EJ:

```
@font-face {
    font-family: 'open_sansregular';
    src: url('../fonts/OpenSans-Regular-webfont.eot');
    src: url('../fonts/OpenSans-Regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('../fonts/OpenSans-Regular-webfont.woff') format('woff'),
         url('../fonts/OpenSans-Regular-webfont.ttf') format('truetype'),
         url('../fonts/OpenSans-Regular-webfont.svg#open_sansregular') format('svg');
    font-weight: normal;
    font-style: normal;
}
```

Evidentemente incluir el soporte a este tipo de archivos en la configuración del url-loader.

```
{
    test: /\.(jpg|jpeg|png|gif|eot|woff|ttf|svg)$/,
    use: {
        loader: 'url-loader',
        options: {
            limit: 150000, // Reflejado en bites
        } 
    }
},
```

Listo, ya puedes asignar las fuentes o fuentes de iconos con el nuevo **font-family** personalizado.

## Manejando videos y file-loader como fallback

La ruta de los videos puede también ser importada desde .jsx para posteriormente usarla

```
import videoPlatzi from '../videos/que-es-core.mp4'

const video = document.createElement('video')
video.setAttribute('src', videoPlatzi)
video.setAttribute('width', 480)
video.setAttribute('controls', true)
document.body.append(video)
```

Al igual que con las imagenes, fuentes y otros archivos multimedia puede ser manejado por el **url-loader** incrustándolo como **base64** pero recuerda que para esto debería establecerse un límite de peso considerable. Normalmente los videos no pesan poco, así que para manejar lo que sucede cuando el peso es superior al definido en el **url-loader**, en él mismo podemos configurar un archivo de salida como fallback. 

```
{
    test: /\.(mp4|webm)$/,
    use: {
        loader: 'url-loader',
        options: {
            limit: 1000000, // Reflejado en bites
            name: '/videos/[name].[ext]'
        } 
    }
},
```

Esto parece estar correcto pero sucede lo siguiente: Cuando el peso excede el límite, ¿quién toma el control del archivo? **file-loader** mediante la opción **name**. Así que debemos también instalar la dependencia.

```
npm install -D file-loader
```

Con esta configuración, si el archivo de video con formato .mp4 o .webm excede los 1000kb, será exportado como un archivo con el nombre y extensión original dentro de la carpeta **videos** que estará ubicada en la **publicPath del output**

```
output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: './dist/',
    filename: 'js/[name].js'
},
```

El **file-loader** se puede también configurar independientemente para leer otros tipos de archivos y definir mayor variedad de opciones.

## Agregando postCss

Aunque ya está un enlace a la documentación en una sección previa, hago un recuento.

Debes instalar el core y el loader de postcss con:

```
npm install -D postcss postcss-loader
```

En el directorio donde se encuentran los archivos css toca crear el achivo de configuración para postCss **postcss.config.js** donde podrás importar los plugins interesantes que quieras usar (como **postcss-preset-env**) usando como base:

```
module.exports = {
    plugins: {
        'plugin-que-quieres-usar': {}
    }
}
```

En *webpack > module > rules* quedaría algo com esto (usando también el MiniCssExtractPlugin.loader), donde la opción **importLoaders:** de css-loader indica que 1 loader será ejecutado en conjunto al css-loader.

```
{
    test: /\.css$/,
    use: [
        MiniCssExtractPlugin.loader,
        //'style-loader', // creates style nodes from JS strings
        {
            loader: 'css-loader', // translates CSS into CommonJS
            options: {
                modules: true,
                importLoaders: 1
            }
        },
        'postcss-loader'
    ],
},
```

## Optimización de código duplicado

Es probable que cuando tengamos distintos entry-points exportemos código duplicado, es decir, código idéntico que pudo ser exportado una sola vez y reutilizado, pero que se importó de forma duplicada por la manera en la que se recibió.

Una forma sencilla de prevenir esto es agreagando a la configuración de webpack

```
optimization: {
    splitChunks: {
        name: "common",
        chunks: "initial"
    }
},
```

Donde se creará un archivo común para el código duplicado y los respectivos archivos independientes por entry-point. De esta manera cada sitio tendría que importar su archivo independiente y el archivo común donde comparte código con el resto.

Documentación:
- https://webpack.js.org/plugins/split-chunks-plugin/
- https://gist.github.com/sokra/1522d586b8e5c0f5072d7565c2bee693

---

**Vendors**

Aparte de lo mencionado antes, también hay una práctica común y es el uso de **vendors**. En este caso lo que hacemos es agrupar las dependencias comunes en un archivo vendor y así las eliminamos de los archivos independientes. De esta forma estas dependencias compartidas que normalmente estarían duplicadas, están en un único archivo. *No es recomendable usar el método anterior y vendor al mismo tiempo, podemos ocasionar más código duplicado*

Primero debemos definir las propiedades en el webpack.config

```
optimization: {
    splitChunks: {
        cacheGroups:{
            vendor:{
                chunks: 'initial',
                name: 'vendor',
                test: 'vendor',
                enforce: true
            }
        }
    }
},
```

Luego debemos definir una entrada adicional, es este caso práctico estamos incorporando a vendor las librerías de react y react-dom.

```
entry: {
    vendor: ['react','react-dom'],
    index: path.resolve(__dirname, 'src/js/index.js'),
    home: path.resolve(__dirname, 'src/js/home.js')
},
```

Así que tendrémos 3 archivos de salida.

- vendor.js que contendrá react y react-dom
- index.js sin react ni react-dom
- home.js sin react ni react-dom

Por último sólo faltaría relacionar los archivos correspondientes a su html.

*ej: a index.html agregar vendor.js e index.js*

## Dynamic Link Libraries : DLL

Ciertamente utilizando vendors optimizamos el empaquetado de nuestras dependencias para el cliente, sin embargo, no estamos optimizando para el proceso de desarrollo, y hay una manera.

Las DLL son archivos que contienen enlaces o referencias a otros archivos o dependencias. En este caso lo que hacemos es crear y compilar una DLL relacionada a las dependencias comunes y asi, durante el proceso de desarrollo, solamente tenemos que compilar los archivos independientes, lo que mejora el iempo de compilado.

En vista de lo anterior, es necesario crear dos scripts, uno que compile nuestra Dll y otro que compile el resto.

```
"build:test": "webpack",
"build:dlls": "webpack --config webpack.dll.config.js",
```

Si, vamos a necesitar un archivo de configuracion diferente para compilar nuestra Dll. Este archivo de configuracion luciria de la siguiente manera:

```
const path = require('path');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
  entry: {
    modules: [
      'react',
      'react-dom',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js',
    library: '[name]',
  },
  plugins: [
    new webpack.DllPlugin({
      name: "[name]",
      path: path.join(__dirname, "[name]-manifest.json")
    })
  ]
}
```

Donde 

- el archivo **modules** definido como **entry** contendra las dependencias comunes a las que quieres referenciar en la dll. 
- **output** indica donde este archivo que contiene las dependencias comunes se exportara. En output, **library** lo que hace es crear una variable global, en este caso, con el nombre "modules" asi es que el navegador puede acceder a las referencias desde cualquier otro elemento.
- Los parametros que recibe el plugin **webpack.DllPlugin** seran: **name** (los nombres que seran exportados como referencias) y **path** (ubicacion y nombre de la Dll con las referencias). La Dll debe llamarse ```[name]-manifest.json```.


**DllPlugin** sirve para crear un archivo con las referencias, archivo que sera interpretado por **DllReferencePlugin**.

Por ultimo, en el archivo de configuracion principal de webpack debemos definirle a los archivos de salida que soliciten referencias a un manifest, esto loa hacemos con el plugin **DllReferencePlugin**.

```
plugins: [
    new webpack.DllReferencePlugin({
        context: path.resolve(__dirname, 'dist'),
        manifest: require('./modules-manifest.json')
    })
],
```

De esta manera los archivos independientes sabran donde buscar las rutas hacia las dependencias comunes.

## Cargando módulos de forma asíncrona

https://platzi.com/clases/1196-webpack/9215-cargando-modulos-asincronamente/

## Configuración para entornos de producción

Llegó el momento de producción, recuerda que podemos tener dos archivos de configuración (o más), uno asignado a cada script. En este caso separaremos la configuración para producción y desarrollo crendo dos archivos.

```
webpack.config.js // para producción

webpack.dev.config.js // para desarrollo
```

Ahora, la diferencia entre estas dos configuraciones será prácticamente la optimización. Por ejemplo, en nuestra configuración para desarrollo puede que nos interese no extraer el css sino dejarlo inline con el style-loader, diferente a la situación en producción. Otra cosa importante es minificar los css y js para producción, cosa que no nos interesa en desarrollo porque necesitamos legibilidad.

Para la optimización de css, aunque webpack 5 tendrá su propio optimizador, actualmente podemos usar el plugin de optimización **optimize-css-assets-webpack-plugin** instalandolo como dependencia de desarrollo.

Cuando usamos esto, se deshabilita la optimización propia de webpack, eso incluye la de JS, por eso también debemos instalar **uglifyjs-webpack-plugin**. El resultado quedaría similar a esto:

```
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true // set to true if you want JS source maps
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].[hash]css",
      chunkFilename: "[id].css"
    })
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader"
        ]
      }
    ]
  }
}
```

**clean-webpack-plugin** es un plugin que podemos instalar para borrar los archivos en un directorio antes de compilar nuevamente asegurando que observando siempre la última versión de los archivos exportados.

```
const plugins = []
plugins.push(
    new CleanWebpackPlugin(['dist'], {root:__dirname})
)
```

Agregar [hash] a los archivos de salida en producción es importante porque cuando estamos haciendo una actualización importante queremos que los usuarios puedan visualizarla y el caché puede impedir eso, por eso procuramos exportar un archivo con un nombre diferente.

## Variable de entorno

La configuración de webpack es puro JS, por lo tanto, pueden manejarse variables y funciones sin problemas. Para desarrollo, normalmente se suelen tener dos entornos, uno local y otro completamente de producción, este último es el que generará todos los nuevos archivos con hashes y completa optimización mientras que el entorno local nos permitiría ver la estructura de nuestros archivos, de nuestro proyecto, sin necesidad de agregar hashes u optimización extrema.

Para poder enviar una variable de entorno a webpack, en el mismo script podemos agregar

```
--env.NODE_ENV=loquesea
```

quedaría por ejemplo

```
"build": "webpack --env.NODE_ENV=production",
"build:local": "webpack --env.NODE_ENV=local"
```

Ahora, esta variable que estamos enviando debe ser recibida de alguna forma por nuestro archivo de configuración. Hasta ahora nuestra configuración tan sólo exporta un objeto

```
module.exports = {
    ...
}
```

Así que para poder recibir esta variable de entorno debemos trabajar con una función que devuelva el objeto:

```
module.exports = (env) => {
    
    return {
    }
}
```
donde el return será el mismo objeto que hemos estado trabajando hasta ahora y antes de este return podemos definir variables, llamadas a otros métodos, condicionales y todo el poder de JS. Justo esto nos va a servir para saber si estamos en el entorno que queremos.

```
module.exports = (env) => {
    
    let mode

    if(env.NODE_ENV === 'production'){

        mode = 'production'
        const plugins = []
        plugins.push(
            new CleanWebpackPlugin(['dist'], {root:__dirname})
        )
    }

    return {
        mode,
        plugins: [
            ...
        ],
        ...
    }
}
```
Esta es una forma bonita de ejemplificar el uso y potencial que esto tiene.