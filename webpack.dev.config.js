const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const path = require('path')
const webpack = require('webpack')

module.exports = {
    mode: 'development',
    entry: {
        //vendor: ['react','react-dom'],
        index: path.resolve(__dirname, 'src/js/index.js'),
        home: path.resolve(__dirname, 'src/js/home.js')
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        publicPath: './dist/',
        filename: 'js/[name].js'
    },
    devServer: {
        port: 9000,
        publicPath: "/dist/",
        open: true
    },
    devtool: 'eval-source-map',
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/[name].css'
        }),
        new webpack.DllReferencePlugin({
            context: path.resolve(__dirname, 'dist'),
            manifest: require('./modules-manifest.json')
        })
    ],
/*     optimization: {
        splitChunks: {
            name: "common",
            chunks: "initial"
        }
    }, */
/*     optimization: {
        splitChunks: {
            cacheGroups:{
                vendor:{
                    chunks: 'initial',
                    name: 'vendor',
                    test: 'vendor',
                    enforce: true
                }
            }
        }
    }, */
    module: {
        rules: [
            {
                test: /\.(css|scss)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    //'style-loader', // creates style nodes from JS strings
                    'css-loader', // translates CSS into CommonJS
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require('sass')
                        }
                    }
                ],
            },
/*             {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    //'style-loader', // creates style nodes from JS strings
                    {
                        loader: 'css-loader', // translates CSS into CommonJS
                        options: {
                            modules: true,
                            importLoaders: 1
                        }
                    },
                    'postcss-loader'
                ],
            }, */
            {
                test: /\.(jpg|jpeg|png|gif|ttf|eot|woff|svg)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 150000, // Reflejado en bites
                        fallback: 'file-loader',
                        name: 'images/[name].[hash].[ext]'
                    } 
                }
            },
            {
                test: /\.(mp4|webm)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 1000000, // Reflejado en bites
                        name: '/videos/[name].[ext]'
                    } 
                }
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-env'],
                    plugins: ['@babel/plugin-proposal-object-rest-spread']
                  }
                }
              }
        ]
    }
}
